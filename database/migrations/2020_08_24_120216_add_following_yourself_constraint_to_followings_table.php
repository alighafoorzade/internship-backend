<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFollowingYourselfConstraintToFollowingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('followings', function (Blueprint $table) {
            DB::statement('ALTER TABLE followings ADD CONSTRAINT chk_following_yourself CHECK (user_id <> following_id)');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('followings', function (Blueprint $table) {
            DB::statement('ALTER TABLE followings DROP CONSTRAINT chk_following_yourself');
        });
    }
}
