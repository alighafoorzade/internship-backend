<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::get('/posts','PostsController@getPosts');
Route::get('/posts/{id}',function($id){
    $post['id'] = $id;
    $post['username'] = 'alighafoorzade';
    $post['image'] = 'https://alighafoorzade.ir/imgaes/1.jpg';
    $post['content'] = 'this is test post content';
    return $post;
});