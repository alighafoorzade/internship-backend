<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PostsController extends Controller
{
    public function getPosts(Request $request){
        $posts = [];
        for( $i = 1; $i <= 202; $i++ ){
            $post['id'] = $i;
            //$post['username'] = 'alighafoorzade';
            switch($i%5){
                case 0:
                    $post['username'] = 'alighafoorzade';
                    break;
                case 1:
                    $post['username'] = 'majidghafoorzade';
                    break;
                case 2:
                    $post['username'] = 'rezaghafoorzade';
                    break;
                case 3:
                    $post['username'] = 'ahmadghafoorzade';
                    break;
                case 4:
                    $post['username'] = 'asgharghafoorzde';
                    break;
            }
            $post['image'] = "https://alighafoorzade.ir/images/a_{$i}.jpg";
            $post['likes'] = 25;
            array_push($posts,$post);
        }
        return $posts;
    }
}
